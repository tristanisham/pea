![](logo.png)
# Pea
The lightweight podcast cms written in Go
---

## About
`pea` is super simple, fast, ulta-portable nano-cms written in Go for your podcast.

It comes with a built in webserver, a theme library, and plugins for storage platforms like Digital Ocean Spaces, S3, and Google Cloud Buckets... or you can just run `init` and it'll generate a project in your current directory.

```fs
$ pea init

./
|- pea.config.json  // Where you configure your podcast
|- src              // Where your website files live
|- audio            // Where your raw podcast files live
```

### Configure your podcast using simple JSON
```json
{
    "title": "",
    "description": "",
    "link": "",
    "pubDate": "2021-12-15T19:42:58.258407939-05:00",
    "updatedDate": "0001-01-01T00:00:00Z",
    "author": {
        "name": "",
        "email": ""
    },
    "podcastArt": "",
    "explicit": false,
    "episodes": [ ]
}
```

### Add a new episode
Drop a new audio file into ```/audio``` and run ```pea gen```. 
```fs
./
|- audio/
|     - test.mp3
```
Results in :
```json
{
    "title": "",
    "description": "",
    "link": "",
    "pubDate": "2021-12-15T19:42:58.258407939-05:00",
    "updatedDate": "0001-01-01T00:00:00Z",
    "author": {
        "name": "",
        "email": ""
    },
    "podcastArt": "",
    "explicit": false,
    "episodes": [
        {
            "episodeTitle": "test",
            "fileName": "test.mp3",
            "episodeDescription": "",
            "episodePubDate": "0001-01-01T00:00:00Z",
            "epsiodeImage": "",
            "episodeSummary": ""
        }
    ]
}
```
Each epsiode will be another array item, and you can edit details manually (in the future through your website). 

### And start the web server!
```fs
$ pea serve
```
Pea comes with a fully production ready web server! If you're running Pea in production, make sure to set a trusted proxy or CDN
```fs

PEA_PROXY=192.80.XX.XX // Set it to your server's static IP address

```
**OR**

```env
PEA_PROXY=192.80.XX.XX;192.60.XX.XX;178.80.XX.XX # Add a list of IPs seperated by a semicolon
```
If you're using Cloudflare or Google Cloud App Engine
set PEA_CDN instead.

```env
PEA_CDN=google // or set it to a trusted CDN IP address.
```

By default, Pea operates on Port :3030, but you can change it using the "-P" flag.
```fs
$ pea serve -P 8080
```
You can also switch to Development mode by including the "-D" flag
```
$ pea serve -D
```
You can also build a static site using `build`
```fs
$ pea build
```