package rss

import (
	"encoding/json"
	"fmt"

	"log"
	"os"

	"github.com/eduncan911/podcast"
	"gitlab.com/tristanisham/pea/data"
)

// JsonToConfig searches "." for 'pea.config.json' and turns it into a PeaConfig struct.
// Throws a fatal error and prints an helpful error message.
func JsonToConfig() pea.PeaConfig {
	data, err := os.ReadFile("pea.config.json")
	if err != nil {
		log.Fatal(err.Error() + ". Try running `$ pea init`")
	}

	res := pea.PeaConfig{}
	if err = json.Unmarshal(data, &res); err != nil {
		log.Fatal(err)
	}
	return res
}

// MakePodcastFromConfigFile takes Pea's config struct and returns the Podcast object.
func MakePodcastFromConfigFile() (*podcast.Podcast, error) {
	configFile := JsonToConfig()

	if configFile.Title == "" {
		return nil, fmt.Errorf("no Title found in config file. Cannot serve podcast without title | %d", 02)
	} else if configFile.Link == "" {
		return nil, fmt.Errorf("no Link found in config file. Cannot serve podcast without link | %d", 03)
	} else if configFile.Description == "" {
		return nil, fmt.Errorf("no Description found in config file. Cannot serve podcast without description | %d", 04)
	}

	p := podcast.New(
		configFile.Title,
		configFile.Link,
		configFile.Description,
		&configFile.PubDate, &configFile.UpdatedDate,
	)

	// add some channel properties
	p.AddAuthor(configFile.Author.Name, configFile.Author.Email)
	p.AddAtomLink(configFile.Link + "/feed")
	p.AddImage(configFile.Link + "/static/" + configFile.PodcastArt + ".jpg")
	p.AddSummary(`link <a href="` + configFile.Link + `">` + configFile.Link + `</a>`)
	if configFile.Explicit {
		p.IExplicit = "yes"
	} else {
		p.IExplicit = "no"
	}
	if len(configFile.Episodes) != 0 {
		for _, item := range AddEpisode(configFile) {
			if _, err := p.AddItem(item); err != nil {
				
				return nil, fmt.Errorf("for podcast episodes, %s", err)
			}
		}
	}

	return &p, nil

}

func AddEpisode(configFile pea.PeaConfig) []podcast.Item {
	episodes := make([]podcast.Item, 0)
	for i, episode := range configFile.Episodes {
		item := podcast.Item{
			Title:       episode.Title,
			Link:        configFile.Link + "/" + episode.FileName,
			Description: episode.Description,
			PubDate:     &episode.PubDate,
		}
		item.AddImage(configFile.Link + "/" + episode.Image)
		item.AddSummary(episode.Summary)
		item.AddEnclosure(configFile.Link+"/static/"+episode.Title+"/"+episode.FileName, podcast.MP3, int64(55*(i+1)))
		episodes = append(episodes, item)
	}
	return episodes
}
