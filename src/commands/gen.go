package commands

import (
	"encoding/json"

	"os"
	"strings"

	"time"

	"gitlab.com/tristanisham/pea/data"
	"gitlab.com/tristanisham/pea/rss"
)

func GenEpisodes() error {
	config := rss.JsonToConfig()
	if err := updateEpisodeList(&config); err != nil {
		return err
	}
	return nil
}

func updateEpisodeList(e *pea.PeaConfig) error {

	files, err := os.ReadDir("./audio")
	if err != nil {
		return err
	}
	episodes := make([]pea.PeaConfigEpisode, 0)
	for _, file := range files {
		if !file.IsDir() {
			token := strings.Split(file.Name(), ".")
			if token[1] == "mp3" || token[1] == "mkv" {
				ep := pea.PeaConfigEpisode{
					Title:       token[0],
					FileName:    file.Name(),
					Description: "",
					PubDate:     time.Time{},
					Image:       "",
					Summary:     "",
				}
				episodes = append(episodes, ep)
			}

		}

	}

	if len(e.Episodes) == 0 {
		e.Episodes = episodes
	} else {
		e.Episodes = append(e.Episodes, episodes...)
	}
	json_out, _ := json.MarshalIndent(e, "", "    ")
	if config_err := os.WriteFile("pea.config.json", json_out, 0666); config_err != nil {
		return config_err
	}
	return nil
}
