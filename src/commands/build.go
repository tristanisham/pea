package commands

import (
	"bytes"
	"html/template"
	"os"

	pea "gitlab.com/tristanisham/pea/data"
	"gitlab.com/tristanisham/pea/rss"
)

func BuildStaticSite() error {
	config := rss.JsonToConfig()
	podcast, err := rss.MakePodcastFromConfigFile()
	if err != nil {
		return err
	}

	if err = os.WriteFile("public/feed.xml", podcast.Bytes(), 0666); err != nil {
		return err
	}

	index, err := os.ReadFile("templates/index.html")
	if err != nil {
		return err
	}

	template, err := template.New("index").Parse(string(index))
	if err != nil {
		return err
	}
	var out bytes.Buffer
	context := pea.Context{
		Title:       podcast.Title,
		Description: podcast.Description,
		Episodes:    config.Episodes,
	}
	template.Lookup("index").Execute(&out, context)

	if err = os.WriteFile("public/index.html", out.Bytes(), 0666); err != nil {
		return err
	}

	return nil
}
