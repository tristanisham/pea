package commands

import (
	"encoding/json"

	"os"
	"time"

	"gitlab.com/tristanisham/pea/data"
)

func Init() error {
	for _, dir := range []string{"public", "audio", "templates"} {
		if err := os.Mkdir(dir, os.ModePerm); err != nil {
			return err
		}
	}

	if err := os.WriteFile("templates/index.html", []byte(pea.IndexHTML()), 0666); err != nil {
		return nil
	}

	json_temp := pea.PeaConfig{
		Title:       "",
		Description: "",
		Link:        "",
		PubDate:     time.Now(),
		UpdatedDate: time.Time{},
		Author: pea.PeaCofigAuthor{
			Name:  "",
			Email: "",
		},
		PodcastArt: "",
		Explicit:   false,
		Episodes:   []pea.PeaConfigEpisode{},
	}

	json_out, _ := json.MarshalIndent(json_temp, "", "    ")
	if err := os.WriteFile("pea.config.json", json_out, 0755); err != nil {
		return err
	}
	return nil
}
