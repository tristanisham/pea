package webserver

import (
	"fmt"

	"log"
	"net/http"
	"os"
	"strings"

	"github.com/gin-gonic/gin"

	"gitlab.com/tristanisham/pea/rss"
)

func WebServerStart(release bool, port string) {
	config := rss.JsonToConfig()
	podcast, configErr := rss.MakePodcastFromConfigFile()
	if configErr != nil {
		log.Fatal(configErr)
	}

	r := gin.Default()
	r.LoadHTMLGlob("templates/*")

	if release {
		gin.SetMode(gin.ReleaseMode)

		if err := setProxy(r); err != nil {
			fmt.Print(err)
		}
	}
	

	r.GET("/", func(c *gin.Context) {
		c.HTML(http.StatusOK, "index.html", gin.H{
			"title": config.Title,
			"desc": config.Description,
			"episodes": config.Episodes,
		})
	})

	r.GET("/rss", func(c *gin.Context) {
		body := podcast.Bytes()

		c.Data(http.StatusOK, "application/xml", body)
	})

	r.GET("/feed", func(c *gin.Context) {
		body := podcast.Bytes()

		c.Data(http.StatusOK, "application/xml", body)
	})

	r.NoRoute(func(c *gin.Context) {
		c.String(http.StatusNotFound, "404")
	})

	r.Run(":" + port)

}

func setProxy(g *gin.Engine) error {

	CDN := os.Getenv("PEA_CDN")
	proxies := os.Getenv("PEA_PROXY")
	if len(proxies) > 0 {
		ips := strings.Split(proxies, ";")
		g.SetTrustedProxies(ips)
		return nil
	} else if len(CDN) > 0 {
		switch strings.ToLower(CDN) {
		case "cloudflare":
			g.TrustedPlatform = gin.PlatformCloudflare
		case "google":
			g.TrustedPlatform = gin.PlatformGoogleAppEngine
		default:
			g.TrustedPlatform = CDN
		}
		return nil
	}

	return fmt.Errorf("no trusted proxy or CDN found. | id: %d", 01)
}
