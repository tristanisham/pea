package pea

func IndexHTML() string {
	return `<!DOCTYPE html>
	<html lang="en">
	
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>{{ .Title }}</title>
		<meta name="description" content="{{ .Description }}">
	</head>
	
	<body>
		<main>
			<h1>{{ .Description }}</h1>
			{{range .Episodes}}
			<article>
				<h1>{{.Title}}</h1>
				<!-- <p>{{.Description}}</p> -->
			</article>
			{{end}}
		</main>
	
	</body>
	
	</html>`
}

type Context struct {
	Title       string
	Description string
	Episodes    []PeaConfigEpisode
}
