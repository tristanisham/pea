package pea

import (
	"time"
)

type PeaConfig struct {
	Title       string             `json:"title"`
	Description string             `json:"description"`
	Link        string             `json:"link"`
	PubDate     time.Time          `json:"pubDate"`
	UpdatedDate time.Time          `json:"updatedDate"`
	Author      PeaCofigAuthor     `json:"author"`
	PodcastArt  string             `json:"podcastArt"`
	Explicit    bool               `json:"explicit"`
	Episodes    []PeaConfigEpisode `json:"episodes"`
}

type PeaCofigAuthor struct {
	Name  string `json:"name"`
	Email string `json:"email"`
}

type PeaConfigEpisode struct {
	Title       string    `json:"episodeTitle"`
	FileName    string    `json:"fileName"`
	Description string    `json:"episodeDescription"`
	PubDate     time.Time `json:"episodePubDate"`
	Image       string    `json:"epsiodeImage"`
	Summary     string    `json:"episodeSummary"`
}
