package main

import (
	"fmt"
	"log"
	"os"

	"gitlab.com/tristanisham/pea/commands"
	"gitlab.com/tristanisham/pea/webserver"
)


func main() {
	args := os.Args[1:]
	if len(args) > 0 {
		switch args[0] {
		case "init":
			if err := commands.Init(); err != nil {
				log.Fatal(err)
			}
		case "build":
			if err := commands.BuildStaticSite(); err != nil {
				log.Fatal(err)
			}
		case "gen":
			if err := commands.GenEpisodes(); err != nil {
				log.Fatal(err)
			}

		case "version":
			fmt.Println("pea", "v0.6.0")
		case "serve":
			release := true
			port := "3030"

			if len(args) > 2 {

				for i, arg := range args {
					switch arg {
					case "-D":
						release = false
					case "-P":
						port = args[i+1]
					}
				}
			}
			
			webserver.WebServerStart(release, port)
		default:
			fmt.Println("pea", "v0.6.0")

			for _, i := range []string{"init", "build", "gen", "version", "serve"} {
				fmt.Println("\t", i)
			}
			fmt.Println("https://gitlab.com/tristanisham/pea/-/blob/master/readme.md")
		}
	}
}

